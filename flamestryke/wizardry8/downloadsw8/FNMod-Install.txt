August 02, 2005

Thank you for giving the new mod a try. Please contact me if you have any difficulty or errors
while playing the game after the mod is installed. I can be reached via email or at the IGN
spoiler board.

flamestrik@aol.com

There are many new things in this mod. New items, monsters, and spells. This means there are several 
files that have to be moved to the proper place. This mod is not compatible with any previous mods
released by myself or any one else to date. (08-02-2005)

This is a self extracting executable file created by WinRAR. Simply download the exe file to the Desktop 
then double-click it. Use the Browse to option and locate the directory where the SirTech folder resides. 
For default installations this would be C:\Program Files folder. For other installations just browse to the drive or folder where the SirTech folder resides, do not select the SirTech folder itself.